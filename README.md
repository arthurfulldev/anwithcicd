# AgWithCICD
&#9745;&#9744;

Este proyecto se construye con el fin de afianzar las técnicas de integración continua para un proyecto de angular con gitlab, cubriendo 4 tareas a realizar por el pipelinee

- &#9745; `DEPENDENCIES` Descarga de dependencias 
- &#9745; `TEST`   revision de pruebas unitarias
- &#9745; `BUILD`  construccion de proyecto listo para producción.
- &#9745; `DEPLOY` crear el deploy del proyecto.

## Indice

* [Home](#AgWithCICD)
* [Configuración chromeHeadless (Paso 1)](#Paso-1:)
* [Script Integration continua (Paso2)](#Paso-2:)
* [stages install y test (Paso 3)](#Paso-3:)

Para este proyecto se utilizara [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

## Configuraciones
Configurando Integracion continua para  un proyecto con angular en para un entorno con gitlab

> ### Paso 1:

Agregar el laucher de chromeHeadless. dentro del archivo karma general. El siguiente codigo se debe pegar despues de la configuracion del browser, dentro de `karma.conf.js`. Ejemplo:

```
  ...

  broesers: ['chrome'],

  ==> inicia codigo nuevo que agregar <==
   
  customLaunchers: {
    ChromeHeadlessCI: {
      base: 'ChromeHeadless',
      flags: ['--no-sandbox']
    }
  },

  ==> Termina codigo nuevo que agregar <==

  ...
```
 > ### Paso 2: 

Agregar el script que debera correr el gtlab para las pruebas de intregracion continua (CI). En el archivo `package.json` de debe agregar el siguiente script:

 ```
    ...
    "test:ci" : "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI",
    ...
 ```

> ### Paso 3:

#### Crear la configuracion para el pipeline de install y test

Primero se debe crear un chivo con el nombre `.gitlab-ci.yml` este es el archivo de configuracion de qitlab para CI/CD

Segundo:
  Agregar la configuracion pertinete para los dos stage, por ejemplo para los stage install puedo agregar las siguiente lineas

```
image: node: 12.18.3

stages:
  - install
  - test
  - build
  - deploy

install:
  stage: install
  script:
    - npm install
  artifacts:
    expire:in: 1h
    paths:
      - node_modules/
  cache:
    path:
      - node_modules

test:
  stage: test
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install
  before_script:
    - apt-get update && apt-get install -y apt-transport-https
    - wget -g -O - https//dl-ssl.google.com/linux/linux_singning_key.pub | apt-key add -
    - sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    - apt-get update && apt-get install -y google-chrome-stable
  script:
    - npm run test:ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'
```

Donde de principio configuro el servidor con el que trabajare, debido a que el proyecto es con angular indico que la imagen sera de node, posterior agrego los stages que quiero que corra mi pipeline, seguido configuro los primero 2 stage (install y test)
